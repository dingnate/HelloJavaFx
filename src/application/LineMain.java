package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class LineMain extends Application {

	@Override
	public void start(Stage primaryStage) {
		
		VBox box = new VBox();
		Scene scene = new Scene(box,300,250);
		scene.setFill(null);
		Line line = new Line(0.0f, 0.0f, 100.0f, 100.0f);
		box.getChildren().add(line);
		primaryStage.setScene(scene);
		primaryStage.show();
		
		
	}

	public static void main(String[] args) {
		launch(args);
	}
}
